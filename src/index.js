import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import axios from 'axios';

import * as serviceWorker from './serviceWorker';

import App from './components/App';
import configureStore from './store/configureStore';
import { initUser } from './actions/userActions';

import './css/style.css';
import './css/bootstrap.min.css';

export const scAxios = axios.create({
    baseURL: 'http://localhost/p-58/api/',
});


const store = configureStore();
store.dispatch(initUser());

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);

serviceWorker.unregister();
