import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { Route, Switch, Redirect } from 'react-router-dom';

import * as userActions from '../../actions/userActions';
import PrivateRoute from '../PrivateRoute.js';
import userdashboard from '../dashboard/userdashboard';

class Dashboard extends React.Component {

    render() {  
        console.log(this.props.user);
        return (
            <>
                <Route/>

                <Switch>
                    <PrivateRoute path='/dashboard' component={userdashboard} user={this.props.user} exact />
                    <Redirect to="/dashboard" />
                </Switch>
                            
            </>
        );
    }
}


const mapStateToProps = (state /*, ownProps*/) => {
    return {
        user: state.user,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        actions: bindActionCreators(userActions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
