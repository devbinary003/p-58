import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { validateUserToken } from '../PrivateRoute';

import Header from './Header.js';
import Footer from './Footer.js';

import mobile_image from '../../images/mobile_image.png';
import p_08 from '../../images/p_08.jpg';
import p_11 from '../../images/p_11.jpg';
import p_13 from '../../images/p_13.jpg';
import phone_1 from '../../images/phone_1.png';


class Home extends React.Component {
  state = {
    };
    
componentDidMount() {
        
}

  render(){
    const { from } = this.props.location.state || { from: { pathname: "/dashboard" } };
    if (validateUserToken()) {
      if (!this.state.user_active)
        return <Redirect to="/dashboard" />
        return <Redirect to={from} />
      }
    
    return (
      <div>
        <Route component={Header} />
        <div className="section-do">
           <div className="container-fluid">
              <div className="row">
                 <div className="col-lg-6 bussines-scn">
                    <p className="learn-more">Learn more<i className="fa fa-long-arrow-right" aria-hidden="true"></i></p>
                    <h4>Business Owners</h4>
                    <h5>A Pointcard for your Business</h5>
                    <p className="jungle-point">It doesn't matter how small your Shop.You can let the customers to get Jungle point from your Busines</p>
                    <div className="section-zero">
                       <p className="expence">Family expense Zero <br />Cost of Maintenance Zero</p>
                       <p className="maintance">Not Only you don't need to pay to join Jungle Point as a businsee owner, but also you don't need to cost the maintenance.</p>
                    </div>
                    <div className="app-dwnld">
                       <h5>App Download is enough to start</h5>
                       <p className="app-need">You don't need to get an extra device to use Jungle Point Card function. Just download Jungle Talk, That's enough.</p>
                    </div>
                    <img src={mobile_image} alt="mobile_image" />
                 </div>
                 <div className="col-lg-6 customer">
                    <p className="learn-more">Learn more<i className="fa fa-long-arrow-right" aria-hidden="true"></i></p>
                    <h4>Customers</h4>
                    <h5>Point is same as Cash in Jungle</h5>
                    <p className="jungle-point">You can use the point as cash wherever the shop is one of Jungle even internationally.</p>
                    <p className="expence">Assigned Fee Zero <br /> Annual Membership Fee Zero</p>
                    <p className="maintance">There's no Fee you need to pay to use Jungle Point Card. All You have to do is using it.</p>
                    <div className="app-dwnld">
                       <h5>Get Points With QR Cord</h5>
                       <p className="app-need">You can use Jungle Point where that is member of Jungle Point Card.</p>
                    </div>
                 </div>
              </div>
           </div>
        </div>
        <div className="jungle-talk">
           <div className="container">
              <div className="row">
                 <div className="col-md-6 col-lg-6">
                    <img src={p_08} alt="p_08" />
                 </div>
                 <div className="col-md-6 col-lg-6">
                    <div className="jungle-card">
                       <p>Jungle point card</p>
                       <h4>Get Jungle Talk to Use Jungle Point Card</h4>
                       <div className="row">
                          <div className="col-md-4 app-store">
                             <a href="/#"><img src={p_11} alt="p_11" /></a>
                          </div>
                          <div className="col-md-4 app-store">
                             <a href="/#"><img src={p_13} alt="p_13" /></a>
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
        <div className="jungle-member">
           <div className="container">
              <div className="row">
                 <div className="col-md-6 col-lg-6">
                    <img src={phone_1} alt="phone_1" className="wow slideInLeft" />
                 </div>
                 <div className="col-md-6 col-lg-6">
                    <div className="shop-card">
                       <h6>Jungle General Point Card</h6>
                       <h4>General card for All Jungle Members</h4>
                       <p className="point-card">Jungle General Point Card, a new Point Card to let you use point internationally on Jungle Membership Business.</p>
                       <p className="g-mark">1. 1% automatically save when use a Shop Card</p>
                       <p className="g-mark">2. Available to charge point by yourself</p>
                       <p className="g-mark">3. You can use any business member which has G Mark.</p>
                    </div>
                 </div>
              </div>
           </div>
        </div>
        <div className="jungle-member card">
           <div className="container">
              <div className="row">
                 <div className="col-md-6 col-lg-6">
                    <div className="shop-card new">
                       <h6>Jungle Shop Point Card</h6>
                       <h4>Shop Card for the Shop user</h4>
                       <p className="point-card lft">Jungle shop Point Card is only for a Business where made up the shop Card.You can use Shop Card for the regularly using business.</p>
                       <p className="g-mark">1. Free Point save from the Membered business.</p>
                       <p className="g-mark">2. Not Available to charge by yourslef</p>
                       <p className="g-mark">3. You can use only the membered shop.</p>
                    </div>
                 </div>
                 <div className="col-md-6 col-lg-6">
                    <img src={phone_1} alt="phone_1" />
                 </div>
              </div>
           </div>
        </div>
      <Route component={Footer} /> 
      </div>
          
    );
  }

}

export default Home;