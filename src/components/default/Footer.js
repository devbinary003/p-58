import React from 'react';
import white_logo from '../../images/white_logo.png';
import twitter from '../../images/twitter.png';
import facebook from '../../images/facebook.png';
import instagram from '../../images/instagram.png';

class Footer extends React.Component {

    render() {

        return (
          <>
          <footer>
           <div className="ftr-section">
              <div className="container">
                 <div className="row">
                    <div className="col-md-6 col-lg-3">
                       <img src={white_logo} alt="white_logo" />
                    </div>
                    <div className="col-lg-6">
                       <ul className="btm-menu">
                          <li>회사안내</li>
                          <li>광고문의</li>
                          <li>사용자이용약관</li>
                          <li>Privacy Policy</li>
                          <li>Contact Us</li>
                       </ul>
                       <ul className="btm-menu content">
                          <li>(00000) 서울특별시 어쩌구 이쪽대로 12길 34 무슨빌딩 </li>
                          <li>TEL: 02-3498-8500</li>
                       </ul>
                       <p className="copyright">© 2019 Simplyonline.io. All rights reserved.</p>
                    </div>
                    <div className="col-md-6 col-lg-3 language">
                       <select name="lang">
                          <option value="">Language</option>
                          <option value="English">English</option>
                          <option value="Japnese">Japnese</option>
                          <option value="Chinese">Chinese</option>
                       </select>
                       <div className="social-icon">
                          <a href="/#"><img src={twitter} alt="twitter" /></a>
                          <a href="/#"><img src={facebook} alt="facebook" /></a>
                          <a href="/#"><img src={instagram} alt="instagram" /></a>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </footer>
          </>
        );
    }
}

export default Footer;
