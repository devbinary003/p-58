import React from 'react';
import Vector_Smart_Objec from '../../images/Vector-Smart-Object.png';
import userImg from '../../images/user.png';

class Header extends React.Component {

    render() {

        return (
          <>
          
          <div className="top-head">
           <div className="container">
              <nav className="navbar navbar-expand-lg navbar-dark">
                 <a className="navbar-brand" href="/#"><img src={Vector_Smart_Objec} alt="Vector_Smart_Objec" /></a>
                 <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                 <span className="navbar-toggler-icon"></span>
                 </button>
                 <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto"></ul>
                    <ul className="navbar-nav mr-right">
                       <li className="nav-item active">
                          <a className="nav-link" href="/#">사용방법</a>
                       </li>
                       <li className="nav-item">
                          <a className="nav-link" href="/#">카드발급</a>
                       </li>
                       <li className="nav-item">
                          <a className="nav-link" href="/#">업소등록 </a>
                       </li>
                       <li className="nav-item">
                          <a className="nav-link" href="/#">적립현황</a>
                       </li>
                       <li className="nav-item user">
                          <a className="nav-link" href="/#"><img src={userImg} alt="userImg" /></a>
                       </li>
                    </ul>
                 </div>
              </nav>
              <div className="genuine-section">
                 <p>A pointcard for your business</p>
                 <h4>Enjoy Varieties</h4>
                 <h6>with Jungle Point Card</h6>
                 <h5>세계 음식점들을 정글카드와 함께 즐기세요.</h5>
              </div>
           </div>
        </div>
              
          </>
        );
    }
}

export default Header;
