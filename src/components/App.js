import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import * as userActions from '../actions/userActions';
import PrivateRoute from './PrivateRoute';
import Home from './default/Home';
import Dashboard from './default/Dashboard';

class App extends Component {
  render() {
    return (
     <Router>
        <Switch>
           <Route exact path="/" component={Home} />
           <Route path='/home' component={Home} />
           <PrivateRoute path='/' component={Dashboard} user={this.props.user} />
        </Switch>
      </Router>
    );
  }
}

const mapStateToProps = (state /*, ownProps*/) => {
  return {
    user: state.user,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(userActions, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);